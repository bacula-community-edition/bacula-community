#!/bin/sh
#
# Copyright (C) 2000-2021 Kern Sibbald
# Copyright (C) 2021-2022 Bacula Systems SA
# License: BSD 2-Clause; see file LICENSE-FOSS
#
# Run a simple backup of the Bacula build directory then do a 
#   Virtual Full backup to another device. Check DeleteConsolidatedJobs
#   and BackupToKeep
#
# This script uses the disk autochanger
#
TestName="virtualfull-plugin-pvf-test"
JobName=Vbackup
. scripts/functions


scripts/cleanup
scripts/copy-migration-confs
echo "${cwd}/build" >${cwd}/tmp/file-list

make -C $cwd/build/src/plugins/fd install-test-plugin

rm -f $cwd/build/inc1 $cwd/build/inc2 $cwd/build/diff1

change_jobname NightlySave $JobName

$bperl -e "add_attribute('$conf/bacula-dir.conf', 'DeleteConsolidatedJobs', 'yes', 'Job', 'Vbackup')" 
$bperl -e "add_attribute('$conf/bacula-dir.conf', 'BackupsToKeep', '3', 'Job', 'Vbackup')"
$bperl -e "add_attribute('$conf/bacula-dir.conf', 'SpoolData', 'no', 'Job', 'Vbackup')"
$bperl -e "add_attribute('$conf/bacula-dir.conf', 'NextPool', 'Full', 'Pool', 'Full')"
$bperl -e "add_attribute('$conf/bacula-dir.conf', 'FileSet', 'PluginSet', 'Job', '$JobName')" 

#$bperl -e "add_attribute('$conf/bacula-dir.conf', 'runafterjob', '/tmp/regress/bin/vbackup.pl  %i %l %p', 'Job', 'Vbackup')"

# Add few more drives
rm -f $tmp/1
for i in 1 2 3 4; do
    $bperl -e "extract_resource('$conf/bacula-sd.conf', 'Device', 'Drive-0')" | sed s/Drive-0/Drive-$i/ >> $tmp/1
done
cat $tmp/1 >> $conf/bacula-sd.conf
$bperl -e "add_attribute('$conf/bacula-sd.conf', 'Device', 'Drive-0,Drive-1,Drive-2,Drive-3,Drive-4', 'Autochanger', 'DiskChanger')"

cat <<EOF >> $conf/bacula-dir.conf
FileSet {
 Name = PluginSet
 Include {
   Plugin = "test-plugin:/@test-plugin@/fr.po:$cwd/build/po/fr.po:/dev/null"
 }
}
EOF

start_test

#
# Note, we first backup into Pool Default, 
#          then Migrate into Pool Full. 
#              Pool Default uses Storage=File
#              Pool Full    uses Storage=DiskChanger

# Write out bconsole commands
cat <<END_OF_DATA >${cwd}/tmp/bconcmds
@output /dev/null
messages
@$out ${cwd}/tmp/log1.out
status client   
setdebug level=100 storage=File trace=1
setdebug level=100 client trace=1
setdebug level=100 tags=sql dir trace=1
label storage=File volume=FileVolume001 Pool=Default
label storage=DiskChanger volume=ChangerVolume001 slot=1 Pool=Full drive=0
label storage=DiskChanger volume=ChangerVolume002 slot=2 Pool=Full drive=0
@# run several jobs
@exec "sh -c 'date > ${cwd}/build/date'"
run job=$JobName level=Full yes
wait
messages
list jobs
@exec "sh -c 'touch ${cwd}/build/inc1'"
run job=$JobName level=Incremental yes
wait
messages
list jobs
@exec "sh -c 'touch ${cwd}/build/inc2'"
run job=$JobName level=Incremental yes
wait
messages
list jobs
@exec "sh -c 'touch ${cwd}/build/inc3'"
run job=$JobName level=Incremental yes
wait
messages
list jobs
@exec "sh -c 'touch ${cwd}/build/inc4'"
run job=$JobName level=Incremental yes
wait
messages
list jobs
@exec "sh -c 'touch ${cwd}/build/inc5'"
run job=$JobName level=Incremental yes
wait
messages
@$out $tmp/count.out
list jobs
.bvfs_get_jobids client=$HOST-fd count
@$out $tmp/log123-ro.out
.jlist pluginrestoreconf objecttype=all jobid=1,2,3
@$out $tmp/log123-obj.out
.jlist object jobid=1,2,3
@$out $tmp/log5.out
run job=$JobName level=VirtualFull yes
wait
messages
@$out $tmp/log7-ro.out
.jlist pluginrestoreconf objecttype=all jobid=7
@$out $tmp/log7-obj.out
.jlist object jobid=7
@$out $tmp/count0.out
list jobs
.bvfs_get_jobids client=$HOST-fd count
@$out $tmp/log5.out
list jobs
@exec "sh -c 'touch ${cwd}/build/inc6'"
run job=$JobName level=Incremental yes
wait
messages
@$out $tmp/count1.out
list jobs
.bvfs_get_jobids client=$HOST-fd count
@$out $tmp/count74-obj.out
.jlist object jobid=7,4
@$out $tmp/log5.out
run job=$JobName level=VirtualFull yes
wait
messages
@$out $tmp/count2.out
list jobs
.bvfs_get_jobids client=$HOST-fd count
@$out $tmp/count3.out
.bvfs_get_jobids jobid=8
@$out $tmp/count9-obj.out
.jlist object jobid=9
@# 
@# now do a restore of the consolidated Full
@#
@$out $tmp/log2.out
restore where=${cwd}/tmp/bacula-restores
5
m *
done
yes
wait
messages
@$out $tmp/log3.out
quit
END_OF_DATA

run_bacula
check_for_zombie_jobs storage=File
stop_bacula

r=`grep -E "^[0-9]+" $tmp/count.out`
if [ "$r" != 6 ]; then
    print_debug "ERROR: Should have 7 jobs (full + incr) in $tmp/count.out"
    estat=2
fi

r=`grep -E "^[0-9]+" $tmp/count0.out`
if [ "$r" != 4 ]; then
    print_debug "ERROR: Should have 4 jobs (vfull + incr) in $tmp/count0.out"
    estat=2
fi

r=`grep -E "^[0-9]+" $tmp/count1.out`
if [ "$r" != 5 ]; then
    print_debug "ERROR: Should have 5 jobs (vfull + 3 incr + incr) in $tmp/count1.out"
    estat=2
fi

r=`grep -E "^[0-9]+" $tmp/count2.out`
if [ "$r" != 4 ]; then
    print_debug "ERROR: Should have 4 jobs (vfull + 3 incr) in $tmp/count2.out"
    estat=2
fi


# 9 is the last virtualfull, 5, 6, 8  are incr, 7 is the restore
grep "9,5,6,8" $tmp/count3.out > /dev/null
if [ $? -ne 0 ]; then
    print_debug "ERROR: Incorrect restore sequence in $tmp/count3.out"
    estat=1
fi

# TODO: Need to check object attributes as well
cat $tmp/log123-ro.out $tmp/log7-ro.out | grep '{'  | perl -MJSON -e '
  $l = <>; 
  $j123 = JSON::decode_json($l) ; 
  $l = <>; 
  $j7 = JSON::decode_json($l) ; 
  exit (scalar(@{$j123->{data}}) == scalar(@{$j7->{data}}));
'
if [ $? != 1 ]; then
    print_debug "ERROR: Should find same number of objects in $tmp/log123-ro.out and $tmp/log7-ro.out"
    estat=1
fi

# Got 3 Objects from the Full and 1 from the Incrmemental
cat $tmp/count74-obj.out | grep '{'  | perl -MJSON -e '
  $l = <>; 
  $j = JSON::decode_json($l) ; 
  exit (scalar(@{$j->{data}}) == 4);
'
if [ $? != 1 ]; then
    print_debug "ERROR: Should find 3 objects in $tmp/count74-obj.out"
    estat=1
fi

# Got 3 Objects from the Full and 1 from the Incrmemental => One overwritten by VF
cat $tmp/count9-obj.out | grep '{'  | perl -MJSON -e '
  $l = <>; 
  $j = JSON::decode_json($l) ;
  ($d) =  grep { $_->{objectcategory} eq "Virtual Machine" } @{$j->{data}};
  exit 2 unless ($d->{objectsize} == 4);
  exit 3 unless (scalar(@{$j->{data}}) == 3);
  exit 1;
'
if [ $? != 1 ]; then
    print_debug "ERROR: Should 3 object from jobid 9 in $tmp/count9-obj.out"
    estat=1
fi

cat $tmp/log123-obj.out $tmp/log7-obj.out  | grep '{'  | perl -MJSON -e '
  $l = <>; 
  $j123 = JSON::decode_json($l);
  exit 1 unless (scalar(@{$j123->{data}}) == 5);     # Must find 5 Objects
  exit 2 unless ($j123->{data}->[0]->{jobid} == 1);  # check the order
  exit 3 unless ($j123->{data}->[1]->{jobid} == 1);  # check the order
  exit 4 unless ($j123->{data}->[2]->{jobid} == 1);  # check the order
  exit 5 unless ($j123->{data}->[3]->{jobid} == 2);  # check the order
  exit 6 unless ($j123->{data}->[4]->{jobid} == 3);  # check the order
  $l = <>;
  $j7 = JSON::decode_json($l);
  exit 7  unless (scalar(@{$j7->{data}}) == 3);     # Must find 3 Objects
  exit 8  unless ($j7->{data}->[0]->{jobid} == 7);  # check the order
  exit 9  unless ($j7->{data}->[1]->{jobid} == 7);  # check the order
  exit 10 unless ($j7->{data}->[2]->{objectsize} == 3);  # Object in Job 1 and 2 are replaced
  exit (0);
' 
ret=$?
if [ $ret != 0 ]; then
    print_debug "ERROR: Got $ret in $tmp/log123-obj.out and $tmp/log7-obj.out analysis"
    estat=1
fi

#
# We only used one log so copy it to the second log
#  so that any restore errors will be picked up
#
check_two_logs
#check_restore_diff

end_test
