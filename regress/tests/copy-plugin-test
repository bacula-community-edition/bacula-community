#!/bin/sh
#
# Copyright (C) 2000-2021 Kern Sibbald
# Copyright (C) 2021-2022 Bacula Systems SA
# License: BSD 2-Clause; see file LICENSE-FOSS
#
# Run a simple backup of the Bacula plugins then copy it
#   to another device.
#
# This script uses the virtual disk autochanger
#
TestName="copy-plugin-test"
JobName=CopyJobSave
. scripts/functions


scripts/cleanup
scripts/copy-migration-confs

make -C $cwd/build/src/plugins/fd install-test-plugin

echo "${cwd}/build" >${cwd}/tmp/file-list
change_jobname NightlySave $JobName
sed 's/migrate/copy/g' $conf/bacula-dir.conf > ${cwd}/tmp/1
sed 's/Migrate/Copy/g' ${cwd}/tmp/1 > $conf/bacula-dir.conf

$bperl -e "extract_resource('$conf/bacula-dir.conf', 'Client', '$HOST-fd')" > $tmp/1

sed "s/$HOST-fd/xxxxx-fd/" $tmp/1 >> $conf/bacula-dir.conf

cat <<EOF >> $conf/bacula-dir.conf
FileSet {
  Name = "Plugins"
  Include {
     Plugin = "test-plugin:/@test-plugin@/fr.po:/tmp/regress/build/po/fr.po:/dev/null"
  }
}
EOF


start_test

#
# Note, we first backup into Pool Default, 
#          then Copy into Pool Full. 
#              Pool Default uses Storage=File
#              Pool Full    uses Storage=DiskChanger

# Write out bconsole commands
cat <<END_OF_DATA >${cwd}/tmp/bconcmds
@output /dev/null
messages
@$out ${cwd}/tmp/log1.out
setdebug level=100 tags=sql trace=1 dir
label storage=File volume=FileVolume001 Pool=Default
label storage=DiskChanger volume=ChangerVolume001 slot=1 Pool=Full drive=0
label storage=DiskChanger volume=ChangerVolume002 slot=2 Pool=Full drive=0
@# run two jobs (both will be copied)
status client=$HOST-fd
run job=$JobName fileset=Plugins yes
wait
messages
list jobs
list volumes
@output $tmp/log-ro1.out
.jlist restoreobjects jobid=1
@output $tmp/log-meta1.out
.jlist metadata type=email tenant=xxxx owner=xxxx jobid=1
@output $tmp/log-object1.out
.jlist object jobid=1
@$out ${cwd}/tmp/log1.out
@#setdebug level=100 dir
@# should copy two jobs
@#setdebug level=51 storage=DiskChanger
@#setdebug level=100 storage=File tags=dedup,asx,network options=h
run job=copy-job jobid=1 yes
wait
messages
@#purge volume=FileVolume001
list jobs 
list volumes
@output $tmp/log-client.out
.jlist restoreobjects client=$HOST-fd
@output $tmp/log-client2.out
.jlist restoreobjects client=xxxxx-fd
@output $tmp/log-ro1-after.out
.jlist restoreobjects jobid=1
@output $tmp/log-meta1-after.out
.jlist metadata type=email tenant=xxxx owner=xxxx jobid=1
@output $tmp/log-object1-after.out
.jlist object jobid=1
@output $tmp/log-ro3.out
.jlist restoreobjects jobid=3
@output $tmp/log-meta3.out
.jlist metadata type=email tenant=xxxx owner=xxxx jobid=3
@output $tmp/log-object3.out
.jlist object jobid=3
@$out ${cwd}/tmp/log3.out
@# 
@# now do a restore
@#
@$out ${cwd}/tmp/log2.out
list volumes 
@#setdebug level=15 director
@#setdebug level=150 storage=DiskChanger
@# Note, here we are restoring from the original backup,
@#  so if you specify storage=DiskChanger the restore will fail
restore where=${cwd}/tmp/bacula-restores fileset=Plugins jobid=3 select client=$HOST-fd
unmark *
mark *
done
yes
list volumes
wait
messages
quit
END_OF_DATA

run_bacula
check_for_zombie_jobs storage=File
stop_bacula

check_two_logs


for i in 1 3; do
    grep '{' $tmp/log-ro$i.out | perl -MJSON -e '$l = <>; $j = JSON::decode_json($l) ; exit (scalar(@{$j->{data}}) == 1);'
    if [ $? != 1 ]; then
        estat=2
        print_debug "ERROR: Should find RestoreObject records for job $i in $tmp/log-ro$i.out"
    fi

    grep '{' $tmp/log-meta$i.out | perl -MJSON -e '$l = <>; $j = JSON::decode_json($l) ; exit (scalar(@{$j->{data}}) == 2);'
    if [ $? != 1 ]; then
        estat=2
        print_debug "ERROR: Should find Metadata records for job $i in $tmp/log-meta$i.out"
    fi

    grep '{' $tmp/log-object$i.out | perl -MJSON -e '$l = <>; $j = JSON::decode_json($l) ; exit (scalar(@{$j->{data}}) == 3);'
    if [ $? != 1 ]; then
        estat=2
        print_debug "ERROR: Should find Object records for job $i in $tmp/log-object$i.out"
    fi
done
#grep 'Backup Level:' tmp/log3.out  | grep Incremental > /dev/null
#if [ $? != 0 ]; then
#    bstat=2
#    print_debug "The incremental job must use copied jobs"
#fi


grep '{' $tmp/log-client.out | perl -MJSON -e '$l = <>; $j = JSON::decode_json($l); exit (scalar(@{$j->{data}}) == 2);'
if [ $? != 1 ]; then
    estat=2
    print_debug "ERROR: Should find 2 RestoreObject records for client $HOST-fd in $tmp/log-client.out"
fi

grep '{' $tmp/log-client2.out | perl -MJSON -e '$l = <>; $j = JSON::decode_json($l); exit (scalar(@{$j->{data}}) == 0);'
if [ $? != 1 ]; then
    estat=2
    print_debug "ERROR: Should find 0 RestoreObject records for client xxxxx-fd in $tmp/log-client2.out"
fi

end_test
