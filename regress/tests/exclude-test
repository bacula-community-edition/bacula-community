#!/usr/bin/env bash
#
# Copyright (C) 2021-2023 Bacula Systems SA
# License: BSD 2-Clause; see file LICENSE-FOSS
#
# Run a simple backup of the Bacula build directory
# with some exclude then restore it.
#
TestName="exclude-test"
JobName=backup
. scripts/functions

scripts/cleanup
scripts/copy-confs

#
# Zap out any schedule in default conf file so that
#  it doesn't start during our test
#
outf="$tmp/sed_tmp"
echo "s%  Schedule =%# Schedule =%g" >${outf}
cp $scripts/bacula-dir.conf $tmp/1
sed -f ${outf} $tmp/1 >$scripts/bacula-dir.conf

cat <<EOF >> $conf/bacula-dir.conf
Fileset {
  Name = ExcludeTest
  Include {
    Options {
       WildFile = *
       Exclude = yes
    }
    File = $cwd/build
  }
}
EOF

cat <<EOF >> $conf/bacula-dir.conf
Fileset {
  Name = ExcludeTest2
  Include {
    Options {
       WildFile = *.po
       Exclude = yes
    }
    File = $cwd/build
  }
}
EOF


change_jobname BackupClient1 $JobName
start_test

cat <<END_OF_DATA >$tmp/bconcmds
@output /dev/null
messages
@$out $tmp/log1.out
setdebug level=4 storage=File1
label volume=TestVolume001 storage=File1 pool=File slot=1 drive=0
run job=$JobName fileset=ExcludeTest yes
wait
messages
run job=$JobName fileset=ExcludeTest2 yes
wait
messages
@# 
@# now do a restore
@#
@$out $tmp/log2.out  
setdebug level=4 storage=File1
restore where=$tmp/bacula-restores select all done fileset=ExcludeTest
yes
wait
messages
@output $tmp/log3.out
list files jobid=2
quit
END_OF_DATA

run_bacula
check_for_zombie_jobs storage=File1
stop_bacula

nb=`find $tmp/bacula-restores -type f | wc -l`
if [ $nb -gt 0 ]; then
    print_debug "ERROR: Should not find files in $tmp/bacula-restores"
    estat=1
fi

nb=`grep fr.po $tmp/log3.out | wc -l`
if [ $nb -gt 0 ]; then
    print_debug "ERROR: Should not find po files in $tmp/bacula-restores2"
    estat=1
fi

check_two_logs
end_test
