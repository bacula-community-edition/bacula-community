#!/bin/sh
#
# Copyright (C) 2000-2021 Kern Sibbald
# License: BSD 2-Clause; see file LICENSE-FOSS
#
# Test for bug #8566 and similar.
# Test if for purged volumes the Volume Protection Time is respected and no error message is printed during trying
# to use such volume.
#
TestName="volume-immutable-purged-test"
JobName=append

. scripts/functions

require_linux

scripts/cleanup
scripts/copy-confs

cleanup()
{
   #Stop the daemon if the test was interrupted in the middle of a run
   sudo systemctl stop test-bacula-sd > /dev/null 2>&1
   #Disable the service
   sudo systemctl disable test-bacula-sd
   #Clear volume attributes if needed
   sudo chattr -ia tmp/*
}

trap 'cleanup' EXIT INT TERM

# Create temporary bacula-sd.service file so that we can run SD as systemd service
scripts/create_sed
sed -f ${tmp}/sed_tmp ${cwd}/build/platforms/systemd/bacula-sd.service.in > ${tmp}/test-bacula-sd.service
sudo systemctl enable ${tmp}/test-bacula-sd.service

# Zap out any schedule in default conf file so that
#  it doesn't start during our test
outf="$tmp/sed_tmp"
echo "s%  Schedule =%# Schedule =%g" >${outf}
cp $scripts/bacula-dir.conf $tmp/1
sed -f ${outf} $tmp/1 >$scripts/bacula-dir.conf

change_jobname BackupClient1 $JobName

start_test

# Set small MaxVolSize so that many volumes are created during backup
$bperl -e 'add_attribute("$conf/bacula-sd.conf", "MaximumVolumeSize", "50M", "Device", "FileChgr1-Dev1")'
$bperl -e 'add_attribute("$conf/bacula-sd.conf", "MaximumVolumeSize", "50M", "Device", "FileChgr1-Dev2")'

# Set some protection directives
$bperl -e 'add_attribute("$conf/bacula-sd.conf", "SetVolumeAppendOnly", "yes", "Device", "FileChgr1-Dev1")'
$bperl -e 'add_attribute("$conf/bacula-sd.conf", "SetVolumeAppendOnly", "yes", "Device", "FileChgr1-Dev2")'
$bperl -e 'add_attribute("$conf/bacula-sd.conf", "SetVolumeImmutable", "yes", "Device", "FileChgr1-Dev1")'
$bperl -e 'add_attribute("$conf/bacula-sd.conf", "SetVolumeImmutable", "yes", "Device", "FileChgr1-Dev2")'

# Run FD and DIR the usual way, SD has to be run with systemd
${bin}/bacula-ctl-fd start
if test $? -ne 0; then
      print_debug "ERROR: Failed to start the FD!"
         dstat=1
fi
sudo systemctl start test-bacula-sd
if test $? -ne 0; then
      print_debug "ERROR: Failed to start the SD!"
      dstat=1
fi
${bin}/bacula-ctl-dir start
if test $? -ne 0; then
      print_debug "ERROR: Failed to start the DIR!"
      dstat=1
fi

# No volume is created manually before running backup job so that Bacula has to create it by itself
cat <<END_OF_DATA >$tmp/bconcmds
@output /dev/null
messages
@$out $tmp/log1.out
run job=$JobName yes
wait
messages
@$out $tmp/log2.out
list volumes
purge volume=Vol-0001
list volumes
@$out $tmp/log3.out
run job=$JobName level=Full yes
wait
messages
list volumes
quit
END_OF_DATA

run_bconsole

backup_res=`cat $tmp/log1.out| grep 'Termination:' | grep 'Backup OK' | wc -l`
if [ ${backup_res} -ne 1 ]; then
   estat=1
   print_debug "ERROR: JobId 1 did not succed, see ${tmp}/log2.out!"
fi

# Check if messages about skipping Immutable volumes was logged
n=`cat $tmp/log3.out| grep "Volume name(s):" | grep "Vol-0003|Vol-0004|Vol-0005" | wc -l`
if [ ${n} -eq 0 ]; then
   estat=1
   print_debug "ERROR: Wrong volumes used for backup, see ${tmp}/log3.out!"
fi

sudo systemctl stop test-bacula-sd
${bin}/bacula-ctl-fd stop
${bin}/bacula-ctl-dir stop

end_test
